This program take one character(1 byte) and encrypt-decrypt it. MAKE SURE YOU DO NOT USE IT TO REALLY ENCRYPT SOMETHING. THIS PROGRAM JUST SHOWS HOW DES WORKS

Compile as:
gcc minides.c -o minides

And run as:
./minides
