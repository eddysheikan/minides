#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>

int CHAR_LENGTH = 8, NUM_KEYS = 3, DEC = 2, ENC = 0, SUCCESS = 0, FAILURE = -1, 
    MAX_VALUE = 255, MIN_VALUE = 0;
char *KEYS[] = {"0011", "1010", "0111"};

void printArr(char *num, int length)
{
  int i;
  for(i = 0; i < length; i++){
    printf("%c", num[i]);
  }
  printf("\n");
}

void binary(int number, char *buff)
{
  int i;
  char result[CHAR_LENGTH];
  unsigned int highestOne = 1 << (CHAR_LENGTH - 1);
  int bit;
  for(i = 0; i < CHAR_LENGTH; i++){
    bit = !!(number & (highestOne >> i));
    result[i] = bit + '0';
  }
  strcpy(buff, result);
}

void character(unsigned int *number, char *buff, int length)
{
  int i;
  *number <<= length - 1;
  unsigned int character;
  for(i = 0; i < length; i++){
    character = buff[(length - 1) - i] - '0' << i ;
    *number |= character; 
  }
}

void getHalf(int numHalf, char *buff, char *src)
{
  int i;
  char result[CHAR_LENGTH/2];
  int start = (CHAR_LENGTH/2) * (numHalf -1);
  for(i = 0; i < CHAR_LENGTH/2; i++){
    result[i] = src[i + start];
  }
  strcpy(buff, result);
}

void crosswire(char *buff)
{
  char result[CHAR_LENGTH/2];
  result[0] = buff[2];
  result[1] = buff[0];
  result[2] = buff[3];
  result[3] = buff[1];
  strcpy(buff, result);
}

void sumKey(char *buff, int key)
{
  char result[CHAR_LENGTH/2];
  int i;
  for(i = 0 ; i < CHAR_LENGTH/2; i++){
    result[i] = (buff[i] ^ KEYS[key][i]) + '0';
  }  
  strcpy(buff, result);  
}

sBox(char *buff)
{
  char result[CHAR_LENGTH/2];
  char *box[16] = {"1010", "0000", "1100", "1000", "0101", "1011", "0011", 
                   "0010", "0001", "1001", "1110", "0100", "0111", "0110", 
                   "1111", "1101"};  
  char *row = (char*)malloc(sizeof(char) * 2);
  char *column = (char*)malloc(sizeof(char) * 2);
  char *cat = (char*)malloc(sizeof(char) * CHAR_LENGTH/2);
  char *sBoxNumber = (char*)malloc(sizeof(char) * CHAR_LENGTH/2);  
  row[0] = buff [0];
  row[1] = buff [3];
  column[0] = buff [1];
  column[1] = buff [2];
  int i;
  for(i = 0; i < CHAR_LENGTH/2; i++){
    if(i < (CHAR_LENGTH/2)/2){
      cat[i] = row[i];
    }else{
      cat[i] = column[i -2];
    }
  }
  unsigned int index = 0;  
  character(&index, cat, CHAR_LENGTH/2);
  strcpy(sBoxNumber, box[index]);
  for(i = 0; i < CHAR_LENGTH/2; i++){
    result[i] = (buff[i] ^ sBoxNumber[i]) + '0';
  }
  strcpy(buff, result);
  free(row);
  free(column);
  free(sBoxNumber);
  row = NULL;
  column = NULL;
  sBoxNumber = NULL;
}

void sum(char *right, char *left)
{
  char result[CHAR_LENGTH/2];
  int i;
  for(i = 0; i < CHAR_LENGTH/2; i++){
    result[i] = (right[i] ^ left[i]) + '0';
  }
  strcpy(right, result);
}

void finalSwap(char *right, char *left, char* buff)
{
  char result[CHAR_LENGTH];
  int i;
  for (i =0; i < CHAR_LENGTH; i++){
    if(i < CHAR_LENGTH/2){
      result[i] = right[i];
    } else {
      result[i] = left[i - CHAR_LENGTH/2];
    }
  }
  strcpy(buff, result);
}

void des(char *binNum, int mode)
{
  int i;
  char *leftHalf = (char *)malloc(sizeof(char) * CHAR_LENGTH/2);
  char *rightHalf = (char *)malloc(sizeof(char) * CHAR_LENGTH/2);
  char *tmp = (char *)malloc(sizeof(char) * CHAR_LENGTH/2);
  getHalf(1, leftHalf, binNum);
  getHalf(2, rightHalf, binNum);
  for (i = 0; i < NUM_KEYS; i++) {
    printf("------------- ITERACION %d -------------\n", i + 1);
    printf("Mitad izquierda: ");
    printArr(leftHalf, CHAR_LENGTH/2);
    printf("Mitad Derecha: ");
    printArr(rightHalf, CHAR_LENGTH/2);
    strcpy(tmp, rightHalf);
    crosswire(rightHalf);
    printf("Crosswire: ");
    printArr(rightHalf, CHAR_LENGTH/2);
    sumKey(rightHalf, abs(mode - i));
    printf("Suma mod 2 con Llave %d (%s): ", abs(mode - i), KEYS[abs(mode - i)]);
    printArr(rightHalf, CHAR_LENGTH/2);
    sBox(rightHalf);
    printf("Caja S: ");
    printArr(rightHalf, CHAR_LENGTH/2);
    sum(rightHalf, leftHalf);
    printf("Suma Der + Izq: ");
    printArr(rightHalf, CHAR_LENGTH/2);
    strcpy(leftHalf, tmp);
    printf("Nueva Izquierda: ");
    printArr(leftHalf, CHAR_LENGTH/2);    
    printf("Nueva Derecha: ");
    printArr(rightHalf, CHAR_LENGTH/2);
  }
  puts("-------------- SWAP FINAL --------------");
  char result[CHAR_LENGTH];
  finalSwap(rightHalf, leftHalf, result);
  printf("Resultado: ");
  printArr(result, CHAR_LENGTH);
  strcpy(binNum, result);
  free(leftHalf);
  free(rightHalf);
  free(tmp);
  leftHalf = NULL;
  rightHalf = NULL;
  tmp = NULL;
}

int main(int argc, char* charv[])
{
  system("clear");
  puts("********************** MINIDES **********************");
  puts("Escribe un caracter ASCII (0-255) a encriptar");
  char c;
  scanf("%c", &c);
  if(c < MIN_VALUE || c > MAX_VALUE){
    puts("Caracter erróneo. Terminando MiniDES...");
    return FAILURE;
  }
  puts("*****************************************************");
  printf("Caracter a encriptar: %c\n", c);
  printf("Valor del caracter: %d\n", c);
  char *binNum = (char *)malloc(sizeof(char) * CHAR_LENGTH);
  binary(c, binNum);
  printf("Binario: ");
  printArr(binNum, CHAR_LENGTH);
  printf("Hexadecimal: %#X\n", c);
  puts("*****************************************************");
  puts("~~~~~~~~~~~~~~~~~~~~ ENCRIPCIÓN ~~~~~~~~~~~~~~~~~~~~~");
  des(binNum, ENC);
  unsigned int number = 0;
  character(&number, binNum, CHAR_LENGTH);
  printf("Caracter encriptado: %c\n", number);
  printf("Valor del caracter: %d\n", number);
  printf("Hexadecimal: %#X\n", number);
  puts("~~~~~~~~~~~~~~~~~~~ DESENCRIPCIÓN ~~~~~~~~~~~~~~~~~~~");
  des(binNum, DEC);
  number = 0;
  character(&number, binNum, CHAR_LENGTH);
  printf("Caracter encriptado: %c\n", number);
  printf("Valor del caracter: %d\n", number);
  printf("Hexadecimal: %#X\n", number);  
  puts("*****************************************************");  
  free(binNum);
  binNum = NULL;
  return SUCCESS;
}
